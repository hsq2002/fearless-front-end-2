function createCard(name, description, pictureUrl, location, starts, ends) {
  return `
  <div class="col-md-4 mb-1">
  <div class="card mb-3 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <div class="card-subtitle"><em>${location}</em></div>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
      ${Date(starts).toString()}
      ${Date(ends).toString()}
      </div>
    </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
        } else {
          const data = await response.json();
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

        }
      } catch (e) {
        // Figure out what to do if an error is raised
      }

    });
